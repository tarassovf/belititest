Rails.application.routes.draw do
  resource :airplane, only: [] do
    collection do
      post :fly_up
    end
  end
  get '/improved', to: 'dashboard#unity'
  root to: 'dashboard#index'
end
