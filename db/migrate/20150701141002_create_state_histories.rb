class CreateStateHistories < ActiveRecord::Migration
  def change
    create_table :state_histories do |t|
      t.integer :airplane_id
      t.string :from
      t.string :to

      t.timestamps null: false
    end
  end
end
