class CreateAirstrips < ActiveRecord::Migration
  def change
    create_table :airstrips do |t|
      t.string :state
      t.integer :airplane_id

      t.timestamps null: false
    end
  end
end
