class CreateAirplanes < ActiveRecord::Migration
  def change
    create_table :airplanes do |t|
      t.string :state
      t.datetime :flyup_time

      t.timestamps null: false
    end
  end
end
