class CreateAirplaneQueues < ActiveRecord::Migration
  def change
    create_table :airplane_queues do |t|
      t.integer :airplane_id

      t.timestamps null: false
    end
  end
end
