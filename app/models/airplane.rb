class Airplane < ActiveRecord::Base
  has_one :airstrip

  FLIGHT_TIME = 10.seconds

  def self.push_to_queue
    plane = create(state: :wait)
    AirplaneQueue.create(airplane_id: plane.id)
    Notifier.new_airplane plane
    QueueWorker.perform_async if AirplaneQueue.count == 1
  end

  def fly_up
    StateHistory.create(airplane_id: id, from: state, to: 'fly_up')
    update_attribute :state, 'fly_up'
    save
    FreeAirstripWorker.perform_in(FLIGHT_TIME, airstrip.id)
    Notifier.fly_up_airplane airstrip
  end

  def flew
    StateHistory.create(airplane_id: id, from: state, to: 'flew')
    self.state = 'flew'
    save
    Notifier.flew self

  end

end
