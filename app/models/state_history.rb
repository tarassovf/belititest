class StateHistory < ActiveRecord::Base
  belongs_to :airplane

  after_create do
    Notifier.new_message self
  end
end
