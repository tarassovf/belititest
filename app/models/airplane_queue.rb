class AirplaneQueue < ActiveRecord::Base
  belongs_to :airplane

  def self.next
    first
  end
end
