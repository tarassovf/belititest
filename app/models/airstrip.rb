class Airstrip < ActiveRecord::Base
  belongs_to :airplane

  def next airplane
    self.airplane_id = airplane.id
    save
    airplane.fly_up
  end

  class << self

    def free id
      airsrtip = find(id)
      airsrtip.airplane.flew if airsrtip.airplane
      airsrtip.airplane_id = nil
      airsrtip.save
      QueueWorker.perform_async
    end

    def get_a_free
      where(airplane_id: nil).first
    end

  end


end
