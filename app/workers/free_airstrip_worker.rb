class FreeAirstripWorker
  include Sidekiq::Worker

  def perform(id)
    Airstrip.free(id)
  end
end
