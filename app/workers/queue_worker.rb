class QueueWorker
  include Sidekiq::Worker

  def perform(*args)
    airstrip = Airstrip.get_a_free
    candidate = AirplaneQueue.next

    if airstrip && candidate
      airstrip.next(candidate.airplane)
      candidate.destroy
    end
  end
end
