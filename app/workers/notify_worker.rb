class NotifyWorker
  include Sidekiq::Worker

  def perform(type, arg)
    Notifier.send(type, arg)
  end
end
