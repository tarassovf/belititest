class AirplanesController < ApplicationController
  def fly_up
    Airplane.push_to_queue
    redirect_to root_url
  end
end
