class DashboardController < ApplicationController
  before_filter :preload_data

  def index
  end

  def unity
    render layout: 'layouts/unity'
  end

  def preload_data
    @history = StateHistory.order(id: :desc)
    @queue = AirplaneQueue.count
  end
end
