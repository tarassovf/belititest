var MessageHandler = function(json){
    var data = JSON.parse(json)
    console.log(data)
    switch(data.type){
        case 'new_airplane':
            return NewAirplane(data);
        case 'new_message':
            return NewMessage(data);
        case 'fly_up':
            return FlyUp(data);
        case 'flew':
            return Flew(data);
    }
}

function NewAirplane(data){
    app.updateQueue(1);
}

function NewMessage(data){
    var li = $('<li></li>');
    li.append(data.id +': '+ data.from + ' -> ' + data.to +' : '+ data.date);
    $('#history').prepend(li);
}

function FlyUp(data){
    app.updateQueue(-1);
    app.updateState(data.state);
    if(app.runned)
        SendMessage('Main Camera', 'FlyUp', '');
}

function Flew(){
    app.updateState('wait');
}
