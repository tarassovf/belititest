# README #

Requirements:

## Ruby version ##
  2.2.2

## System dependencies ##
  node.js redis

### Debian
  sudo apt-get install nodejs redis

### Mac OS ###
  brew install node

  brew install redis

## Configuration ##
  bundle install

  ./bin/setup


## Database creation ##
  rake db:setup

## Database initialization ##
  rake db:seeds

## Services (job queues, cache servers, search engines, etc.) ##
  redis-server

  bundle exec sidekiq

  node lib/notifier/notifier.js
