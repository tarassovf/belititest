require 'httparty'

class Notifier
  include HTTParty
  base_uri 'localhost:8000'

  class << self
    def new_airplane airplane
      @params = { type: 'new_airplane', id: airplane.id, state: airplane.state }
      send
    end

    def fly_up_airplane airstrip
      @params = { type: 'fly_up', id: airstrip.id, airplane: airstrip.airplane.id, state: airstrip.airplane.state }
      send
    end

    def flew airplane
      @params = { type: 'flew', id: airplane.id, state: airplane.state }
      send
    end

    def new_message history
      @params = { type: 'new_message', id: history.airplane_id,
                  from: history.from, to: history.to,
                  date: history.created_at.strftime("%d.%m.%y %H:%M:%S") }
      send
    end

    def send
      post("", body: @params.to_json)
    end
  end
end
