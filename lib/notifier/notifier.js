var fs = require('fs')

var webSocketServer = require('websocket').server
var server = require('./http_server')
var ClientList = require('./client_list')

var config = {
  port: 8000
}

server.listen(config.port, function() {
  console.log((new Date()) + " Server is listening on port " + config.port)
})

server.on('error', function(e) {
    console.log(e)
})

var wsServer = new webSocketServer({
  httpServer: server
})

wsServer.on('request', function(request) {
  var connection = request.accept(null, request.origin)

  ClientList.addClient(connection)

  connection.on('message', function(message) {
    try{
      ClientList.broadcast(message)
    }catch(e){
      console.log(e.stack)
    }
  });

  connection.on('close', function(reasonCode, description) {
    ClientList.disconnect(this)
  })
})
