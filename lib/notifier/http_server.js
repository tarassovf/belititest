var http = require("http")
var ClientList = require('./client_list')

var server = http.createServer(function(request, response) {
  if(request.url == '/'){
    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;

            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
          ClientList.broadcast(body)
          response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
          response.end('')
        });
    } else {
      response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
      response.end('')
    }
  }
});

module.exports = server
